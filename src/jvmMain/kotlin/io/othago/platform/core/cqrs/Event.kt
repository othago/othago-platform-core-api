/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.assert.InfrastructureConfigurationCodeAssert
import io.othago.platform.common.version.Version
import kotlinx.serialization.Serializable
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class ActionDef(val ftrRef : String = DEFAULT_FTR_REF, val actionName : String, val ver : String = "0.0.0")
fun ActionDef.toCommandActionId() = CommandActionId(FeatureId.decode(ftrRef),actionName,Version.decode(ver))
fun ActionDef.toReactActionId() = ReactActionId(FeatureId.decode(ftrRef),actionName,Version.decode(ver))
fun ActionDef.toQueryActionId() = QueryActionId(FeatureId.decode(ftrRef),actionName,Version.decode(ver))

fun <E : GenericEvent<*>> retrieveActionDef(eventClass : KClass<E>): ActionDef {
    val actionAnnotation = eventClass.findAnnotation<ActionDef>()
    InfrastructureConfigurationCodeAssert.assertNotNull(actionAnnotation,"Action annotation [@${ActionDef::class.qualifiedName!!}] is required for this event [${eventClass.qualifiedName!!}] since it's a subclass of generic Event [${GenericEvent::class.qualifiedName}]")
    return actionAnnotation!!
}

@Suppress("UNCHECKED_CAST")
@Serializable
sealed class GenericEvent<out ACT : ActionId> : IPlatformEvent<ACT> {
    private val actionId : ACT = determineActionId()
    override fun retActionId() = actionId
    private fun determineActionId() : ACT {
        val actionDef = retrieveActionDef(this::class)
        return when(this){
            is GenericCE -> actionDef.toCommandActionId() as ACT
            is GenericQE -> actionDef.toQueryActionId() as ACT
            is GenericRE -> actionDef.toReactActionId() as ACT
        }
    }
}

@Serializable
open class GenericCE : GenericEvent<CommandActionId>(), ICommandEvent
@Serializable
open class GenericRE : GenericEvent<ReactActionId>(), IResultEvent
@Serializable
open class GenericQE : GenericEvent<QueryActionId>(), IQueryEvent
