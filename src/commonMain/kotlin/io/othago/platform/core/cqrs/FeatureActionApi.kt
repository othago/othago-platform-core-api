/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.assert.InfrastructureConfigurationCodeAssert
import io.othago.platform.common.version.Version
import kotlinx.serialization.Serializable

const val DEFAULT_FTR_NAME = "DEFAULT_FTR"
const val DEFAULT_FTR_REF = "DEFAULT_FTR:0.0.0"
val DEFAULT_FTR = FeatureId(DEFAULT_FTR_NAME)

@Serializable
open class FeatureId(val featureName: String = DEFAULT_FTR_NAME, val ver : Version = Version()) : Comparable<FeatureId>{
    companion object{
        const val FTR_ID_PATTERN = "[a-zA-Z_-]\\w*:\\d\\.\\d\\.\\d"
        fun decode(ftrIdStr : String) : FeatureId {
            InfrastructureConfigurationCodeAssert.assertTrue(
                ftrIdStr.matches(FTR_ID_PATTERN.toRegex()),
                "FeatureId pattern can't be matched for input [ftrIdStr=$ftrIdStr,pattern=$FTR_ID_PATTERN]")
            return ftrIdStr.split(":").toList().let { FeatureId(it[0], Version.decode(it[1])) }
        }
    }
    fun ref() = "$featureName:${ver.code()}"

    override fun equals(other: Any?): Boolean {
        return other is FeatureId && this.featureName == other.featureName && this.ver == other.ver
    }

    override fun compareTo(other: FeatureId): Int {
        if (this.featureName == other.featureName) return this.ver.compareTo(other.ver)
        return this.featureName.compareTo(other.featureName)
    }

    override fun hashCode() = 31 * (31 + featureName.hashCode()) + ver.hashCode()

    override fun toString() = ref()
}

enum class ActionType {
    COMMAND, QUERY, REACT;
}

@Serializable
sealed class ActionId(
    val featureId: FeatureId,
    val actionName: String,
    val ver: Version,
    val type: ActionType,
) : Comparable<ActionId> {

    override fun equals(other: Any?): Boolean {
        return other is ActionId && this.featureId == other.featureId &&
                this.actionName == other.actionName && this.ver == other.ver && this.type == other.type
    }

    override operator fun compareTo(other: ActionId): Int {
        if(this.featureId == other.featureId) {
            if (this.actionName == other.actionName){
                if (this.ver == other.ver) return this.type.compareTo(other.type)
                return this.ver.compareTo(other.ver)
            }
            return this.actionName.compareTo(actionName)
        }
        return this.featureId.compareTo(other.featureId)
    }

    override fun hashCode() = 31 * (31 * (31 + featureId.hashCode()) + actionName.hashCode()) + ver.hashCode()

    override fun toString() = "<ftr=$featureId,action=$actionName,ver=$ver,type=$type>"
}

@Serializable
sealed class CallableActionId(featureId : FeatureId, actionName : String, ver : Version, type : ActionType)
    : ActionId(featureId,actionName,ver,type) //OUT dependency marker class

@Serializable
open class CommandActionId(featureId : FeatureId = DEFAULT_FTR, actionName : String, ver : Version = Version())
    : CallableActionId(featureId,actionName,ver,ActionType.COMMAND)

@Serializable
open class ReactActionId(featureId : FeatureId = DEFAULT_FTR, actionName : String, ver : Version = Version())
    : ActionId(featureId,actionName,ver,ActionType.REACT)

@Serializable
open class QueryActionId(featureId : FeatureId = DEFAULT_FTR, actionName : String, ver : Version = Version())
    : CallableActionId(featureId,actionName,ver,ActionType.QUERY)
