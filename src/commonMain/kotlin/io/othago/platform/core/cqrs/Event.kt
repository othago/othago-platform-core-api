/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.version.Version
import kotlinx.serialization.Serializable

interface IPlatformEvent<out ACT : ActionId>{
    fun retActionId() : ACT
    fun sig(): String = "${this::class.simpleName}:${retActionId()}"
}

/* marker interfaces */
interface ICommandEvent : IPlatformEvent<CommandActionId>
interface IResultEvent : IPlatformEvent<ReactActionId>
interface IQueryEvent : IPlatformEvent<QueryActionId>

/* all extension props must be provided in actual event impl class
 -> serialization & passing via constructor issue */
@Serializable
sealed class PlatformEvent<ACT : ActionId>(val actionId : ACT) : IPlatformEvent<ACT> {
    override fun retActionId() = actionId
}

@Serializable
open class CommandEvent(actionId : CommandActionId) : PlatformEvent<CommandActionId>(actionId), ICommandEvent{
    constructor(featureId : FeatureId = DEFAULT_FTR, actionName : String, ver : Version = Version()) : this(CommandActionId(featureId,actionName,ver))
}
@Serializable
open class ResultEvent(actionId : ReactActionId) : PlatformEvent<ReactActionId>(actionId), IResultEvent{
    constructor(featureId : FeatureId = DEFAULT_FTR, actionName : String, ver : Version = Version()) : this(ReactActionId(featureId,actionName,ver))
}
@Serializable
open class QueryEvent(actionId : QueryActionId) : PlatformEvent<QueryActionId>(actionId), IQueryEvent{
    constructor(featureId : FeatureId = DEFAULT_FTR, actionName : String, ver : Version = Version()) : this(QueryActionId(featureId,actionName,ver))
}
