/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable

interface IQueryResult

@Serializable
abstract class ProcessingResult

@Serializable
data class CommandResult(val payload: List<@Polymorphic IResultEvent>) : ProcessingResult()

@Serializable
data class QueryResult(@Polymorphic val payload: IQueryResult) : ProcessingResult()

@Serializable
data class ErrorParam(val key: String, val value: String)

@Serializable
data class ErrorItem(val exCode: String, val traceMsg: String, val params: List<ErrorParam> = emptyList())

@Serializable
data class ErrorResult(val errors: List<ErrorItem> = emptyList()) : ProcessingResult() {
    constructor(errorItem: ErrorItem) : this(listOf(errorItem))
}
